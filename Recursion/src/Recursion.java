/**
 * This code is just to test 2 different recursive solutions to problems.
 * @author willhelmbrecht
 *
 */
public class Recursion {
    
    /**
     * Given base and n that are both 1 or more, compute recursively (no loops) 
     * the value of base to the n power, so powerN(3, 2) is 9 (3 squared).
     * 
     * @param base an int with the base number.
     * @param n an int with the power for base to be taken to.
     * 
     * @return an int with the final number.
     */
    public static int powerN(int base, int n) {
        // if the power is down to 0;
        if (n == 0) {
            return 1;
        }
        
        //take the base and multiply by the recursive method
        //to do it again with n lowered by 1 to go towards
        //the base case.
        return base * powerN(base, n - 1);
        
    }
    
    /**
     * We have triangle made of blocks. The topmost row has 1 block, 
     * the next row down has 2 blocks, the next row has 3 blocks, and 
     * so on. Compute recursively (no loops or multiplication) the 
     * total number of blocks in such a triangle with the given number of rows.
     * 
     * @param row The number of rows in triangle.
     * 
     * @return an int with the final number of blocks.
     */
    public static int triangle(int row) {
        // if row is 0
        if (row == 0) {
            return 0;
        }
        
        //add row to recursive method with 1 less row
        //each time its run
        return row + triangle(row - 1);
    }
}
